<?php

/**
 * Class Home
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Printer extends Controller
{
    public function index(){
        require 'application/views/_templates/miniheader.php';    
        echo 'No page to print...';
        require 'application/views/_templates/minifooter.php';    
    }

    public function page($imageName='')
    {
        if(file_exists('public/img/'.$imageName.'.jpg')){
            
            require 'application/views/_templates/autoprintheader.php';  
            echo $this->dressTemplate('/_templates/autoprint',
                                        array('imageURL' => URL . 'public/img/'.$imageName.'.jpg')
                                     );
        }else{
            require 'application/views/_templates/miniheader.php';            
            echo 'No page to print...';
        }      
        
        require 'application/views/_templates/minifooter.php';
    }


}