<!--index.php-->

<div id="wrapper"><!--The document wrapper that contains the various DOM elements-->
	
	<!--<img class="postit" src="<?php echo URL ?>public/img/postit.png">				
	-->
	<img id="p1" class="polaroid-stack" src="<?php echo URL ?>public/img/stack1.png">
	<img id="p2" class="polaroid-stack" src="<?php echo URL ?>public/img/stack2.png">
	<img id="p3" class="polaroid-stack" src="<?php echo URL ?>public/img/stack3.png">
	<img id="p4" class="polaroid-stack" src="<?php echo URL ?>public/img/stack4.png">
	<img id="p5" class="polaroid-stack" src="<?php echo URL ?>public/img/stack5.png">
	<img id="p6" class="polaroid-stack" src="<?php echo URL ?>public/img/stack6.png">
	<img id="p7" class="polaroid-stack" src="<?php echo URL ?>public/img/stack7.png">

	<!--<img class="polaroid tilt-5-left" src="public/img/polaroid.png" alt="polaroid">					
	-->
	<!--The envelope piece behind the pages-->	
	<img id="envelope-tophalf" class="envelope" src="public/img/envelope-tophalf.png" alt="envelope">				

	<!--The envelope piece in front of the pages-->	
	<img id="envelope-bottomhalf" class="envelope" src="public/img/envelope-bottomhalf.png" alt="envelope-bottomhalf">

	<!--The print button-->
	<a id="printBtn" class="pageBtn" title="Skriv ut denna sida" href="#"><img src="<?php echo URL ?>public/img/printer.png" alt="Printer"></a>

	<!--Page number 1 (Coverletter) -->	
	<img id="coverletter" class="folded-up page" src="public/img/coverletter.jpg" alt="coverletter">	

	<!--Page number 1 (CV) -->	
	<img id="cv" class="folded-up page second-page" src="<?php echo URL ?>public/img/cv.jpg" alt="cv">	

</div><!--End of wrapper-->

<!--End of index.php-->