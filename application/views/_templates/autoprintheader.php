<!DOCTYPE html>
<html lang="sv">
	<head>
	    <meta charset="utf-8">
	    <title>Autoprint</title>		
	    <!--Google Analytics-->
	    <?php include_once('application/utillities/analyticstracking.php') ?> 		
		<script>
			window.onload = function(){
				window.focus();
				window.print();
			};
		</script>
	</head>	
	<body>