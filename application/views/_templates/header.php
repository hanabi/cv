<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>CV - Sebastian Lockwood</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!--Google Analytics-->
    <?php include_once('application/utillities/analyticstracking.php') ?>    

    <!--MouseFlow tracking-->
    <?php include_once('application/utillities/mouseflow.php') ?>    


    <!-- jQuery -->
    <script src="public/js/jquery-1.11.0.min.js"></script>

    <!-- Eric Mayers Reset -->
    <link href="public/css/mayersreset.css" rel="stylesheet">

    <!-- Our Animations -->
    <link href="public/css/animations.css" rel="stylesheet">

    <!-- General CSS -->
    <link href="public/css/style.css" rel="stylesheet">
   
    <!-- Javscript file for handling the CV -->
    <script src="public/js/cv.js"></script>

    <!-- Javscript file for handling dynamic CSS -->
    <script src="public/js/dynamic-css.js"></script>

</head>
<body>
<!--End of Header.php-->
