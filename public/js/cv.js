//This JS file handles all the intricacies of the DOM manipulation	

	// Waits untill all images/elements are loaded
	// Then fires up the DOM
	$(window).load(function() {
		//Makes the DOM visable and initialises click events.
		initDOM();
	});

	function initDOM(){
		//Fade all the elements into view
	    $('#wrapper').fadeIn(1000);
	    
	    //Makes sure that if user tests the responsive design
	    //and resizes the window, the pages should not be missalligned.
	    alignPagesToEnvelope();
	    
	    //Make's the pages inside the envelope clickable
	    initPageClickEvents();
	    
	    //Sometimes people click the envelope to get page one.
	    //So lets give it to them then!
	    initEnvelopeClickEvent();

	    //Init Polaroid click event
	    //initPolaroidClickEvent();

	    //Starts trowing the polaroids into view
	    //After a short delay
	    setTimeout(function() {
			initPolaroidStackEvents();	
	    }, 2000);
	    

	    //Init Postit hover event
	    //initPostitHoverEvent();

	    //Init the click events for buttons like Print on pages
	    initPageButtonClickEvents();

	};

	//When the window is resized, allign the pages to the envelope
	window.onresize = function(){
		alignPagesToEnvelope();
		
		//If a page is out, allign the print button
		if($('.move-out')){
			allignPageButton($('.move-out'));
		}
		
	};   

	//Alligns the pages to the bottom-half of the envelope. 
	//Its quicker then to check against both parts of the envelope.
	function alignPagesToEnvelope(){
		var currentEnvelopeLeft=$("#envelope-bottomhalf").css("margin-left");
		$(".page").css("margin-left",currentEnvelopeLeft);
	};

	//If you click in the transparent 'V' of the envelope you will
	//think you clicked the page but really it was the transparent envelope.
	//So if you click the envelope, simulate that you clicked the page instead. 
	function initEnvelopeClickEvent(){
	    $('#envelope-bottomhalf').click(function(){
			//if the first page is in the envelope, move it out
			if(!$('#cv').hasClass("move-out")){
				$('#cv').removeClass("move-in");
				$('#cv').removeClass("folded-up");
				$('#cv').addClass("move-out");
				
				//when animation has finished allign and display print button
				showPageBtn();
			}
		});
	};

	//Loads up the click events for the pages
	function initPageClickEvents(){
	    $('.page').click(function(){
			//if page is out of the envelope, move it in
			if($(this).hasClass("move-out")){
				$(this).removeClass("move-out");
				$(this).addClass("folded-up");
				$(this).addClass("move-in");

				$('.pageBtn').css('display','none');	

			}
			//if page is in the envelope, move it out
			else if($(this).hasClass("move-in")){
				$(this).removeClass("move-in");
				$(this).removeClass("folded-up");
				$(this).addClass("move-out");
		
				//when animation has finished allign and display print button
				showPageBtn();
			}
			//This only runs the first time a page is selected
			else{
				$(this).addClass("move-out");
				$(this).removeClass("folded-up");

				//when animation has finished allign and display print button
				showPageBtn();
			}
		});
	};

	//Loads the click event for the polaroid image
	function initPolaroidClickEvent(){
		$('.polaroid').click(function(){
			
			//If the picture loaded is x, select the next one
			switch($('.polaroid').attr('src')){
				case 'public/img/polaroid.png':
					$('.polaroid').attr('src','public/img/polaroid1.png');
					break;
				
				case 'public/img/polaroid1.png':
					$('.polaroid').attr('src','public/img/polaroid2.png');
					break;	
				
				case 'public/img/polaroid2.png':
					$('.polaroid').attr('src','public/img/polaroid3.png');
					break;					
				
				//If the last one was clickd, load the first one
				case 'public/img/polaroid3.png':
					$('.polaroid').attr('src','public/img/polaroid.png');
					break;				
			}
		});
	}

	//Loads the mouse mover events for the postit
	//
	//We use cloning to preload the image so it it's added seamlesly
	//even on a slow connection.
	function initPostitHoverEvent(){

		$('.postit').mouseenter((function(){
				
			//On mouse hover if there is not allready a clone then 
			//copy the old image change it to one with ticks
			if(!$('.clone').length){
				//Clone the current picture and put it in a temp variable
				var postitTicks = $('.postit').clone();
				
				//Change the picture in the clone
				$(postitTicks).attr('src','public/img/postit-ticks.png');
				
				//Make the clone invisible so we can fade it in later
				$(postitTicks).addClass('clone').css('display','none');
				
				// We skip the part where we let the postit
				// return to its initial state.	
			/*
				// When mouse pointer hovers somewhere else kill img clones
				$(postitTicks).mouseleave((function(){
					$('.clone').remove();				
				}));
			*/	
				//Attach the clone to the DOM
				//Since it's a clone it will have the same position
				//So it is inserted on top of the old one.
				$(postitTicks).appendTo('#wrapper');
				
				//Fade the changed picture in!
				$(postitTicks).fadeIn(1000);
			}
		}));//end of mouseover function
	}//end of HoverEvent function

	function initPageButtonClickEvents(){
		$('#printBtn').click(function(){	
			printPage($('.move-out').attr('id'));
		});
	}
	
	function initPolaroidStackEvents(){

		var numOfPolaroids = $('.polaroid-stack').length;
		var nextAnimExecute = 0;
		
		//loops over all the polaroids and sets each fade-in on a timer
		for(var i=1; i<numOfPolaroids+1; i++) {
			(function(i) {
				setTimeout(function() { 
					var selector='#p' +i;
					var value='';

					
					//Give pages a different position to each other
					//Last one at the bottom
					if(i==7){
						value =  'bottom:'+5+'%!important;';
					}else{
						value =  'bottom:'+((i*11)-11)+'%!important;';
					}
					
					
					//Make them visible
					//value += 'display:block;';
					
					//generate two columns of random alignment
					//So on every other turn give random values for each column
					if((i % 2) !== 0){
						value += 'right:'+ Math.floor(Math.random() * 10+10)+'%;';
					}
					else{
						value += 'right:'+ Math.floor(Math.random() * 10)+'%;';
					}
					
					//Add a random tilt to the images
					rndTilt=Math.floor(Math.random() * 30 - 15);
					value += 'transform: rotate('+rndTilt+'deg); -o-transform: rotate('+rndTilt+'deg);-moz-transform: rotate('+rndTilt+'deg);-webkit-transform: rotate('+rndTilt+'deg);-ms-transform: rotate('+rndTilt+'deg);';
					
					//As defined in dynamic-css.js
					addCSSRule(document.styleSheets[0],selector,value,1); 

					$(selector).fadeIn();
			    }, nextAnimExecute);
			})(i);
			
			nextAnimExecute+=650;
		}
	}

	//Creates an iframe that prints the current image
	//instead of the whole webpage.
	function printPage(page){
		console.log(page);
		if(typeof page == 'undefined'){
			console.log('invalid param to printPage...');				
			return false;
		}	
		var iFrame = document.createElement("IFRAME");
		
		//Load the auto-print page on the server that prints a
		//html page with the specified image. 
		iFrame.setAttribute('src', 'printer/page/' + page);
		
		//Couldn't figure out how to just print from the iframe in memory
		//Temporary solution at best to just make it 'invisible'.
		iFrame.setAttribute('display','none');
		iFrame.setAttribute('visibility','hidden');
		iFrame.style.width = 0+"px";
		iFrame.style.height = 0+"px"; 
		
		//Sigh, 
		//stick the iframe to to DOM so it runs the autoprinting proccess
		$(iFrame).appendTo('body');
	} 

	function allignPageButton(parentPage){
		if(typeof parentPage == 'undefined'){
			console.log('invalid param to allignPageButton...');			
			return false;
		}
		
		var pageMarginLeft = strToFloat($(parentPage).css('margin-left'))-250;
		var pageLeft = strToFloat($(parentPage).css('left'));
		
		$('.pageBtn').css('margin-left',(pageMarginLeft + pageLeft +275)+'px');
	};

	function showPageBtn(){
		setTimeout(function(){
			//If there is a page out of the envelope when the timer is up
			if($('.move-out').length){
				//Then allign and fade in the button
				allignPageButton($('.move-out'));
				$('.pageBtn').fadeIn(1000);
			}
		},2500);
	}

	function strToFloat(str){
		if(!str){
			console.log('invalid param to strToFloat...');
			return false;
		}
		return parseFloat(str, 10);
	}